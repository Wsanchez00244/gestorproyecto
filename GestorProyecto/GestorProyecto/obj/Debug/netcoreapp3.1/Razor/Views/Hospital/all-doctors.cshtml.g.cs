#pragma checksum "E:\Wilson Sanchez Personal\Universidad Fidelitas\Proyecto graduacion\GestorProyecto\GestorProyecto\GestorProyecto\Views\Hospital\all-doctors.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1c500bdee8b37fc4d0dff9e843fc5dfe1bc7b29b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Hospital_all_doctors), @"mvc.1.0.view", @"/Views/Hospital/all-doctors.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\Wilson Sanchez Personal\Universidad Fidelitas\Proyecto graduacion\GestorProyecto\GestorProyecto\GestorProyecto\Views\_ViewImports.cshtml"
using GestorProyecto;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\Wilson Sanchez Personal\Universidad Fidelitas\Proyecto graduacion\GestorProyecto\GestorProyecto\GestorProyecto\Views\_ViewImports.cshtml"
using GestorProyecto.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1c500bdee8b37fc4d0dff9e843fc5dfe1bc7b29b", @"/Views/Hospital/all-doctors.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"29b84e1045f78866cbc2ebcccdcc7ccced17bae5", @"/Views/_ViewImports.cshtml")]
    public class Views_Hospital_all_doctors : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/users/dr-2.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("user"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("rounded-circle mt-n3"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/users/dr-4.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/users/dr-5.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/users/dr-6.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/images/users/dr-1.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<!-- Page-Title -->
<div class=""row"">
    <div class=""col-sm-12"">
        <div class=""page-title-box"">
            <div class=""float-right"">
                <ol class=""breadcrumb"">
                    <li class=""breadcrumb-item""><a href=""javascript:void(0);"">Metrica</a></li>
                    <li class=""breadcrumb-item""><a href=""javascript:void(0);"">Doctors</a></li>
                    <li class=""breadcrumb-item active"">Doctors</li>
                </ol>
            </div>
            <h4 class=""page-title"">All Doctors</h4>
        </div><!--end page-title-box-->
    </div><!--end col-->
</div>
<!-- end page title end breadcrumb -->
<div class=""row"">
    <div class=""col-lg-3"">
        <div class=""card"">
            <div class=""card-body text-center"">
                <div class=""text-right"">
                    <a href=""#"" class=""mr-2"" data-toggle=""modal"" data-animation=""bounce"" data-target="".bs-example-modal-lg""><i class=""fas fa-edit text-info font-16""></i></a>
                    <a h");
            WriteLiteral("ref=\"#\"><i class=\"fas fa-trash-alt text-danger font-16\"></i></a>\r\n                </div>\r\n                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "1c500bdee8b37fc4d0dff9e843fc5dfe1bc7b29b7042", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                <h5 class=""mb-1 client-name"">Dr.Wendy Keen</h5>
                <p class=""text-muted"">MD Orthopedic</p>
                <p class=""text-center font-14"">4 years experience in Apollo Hospital</p>
                <button type=""button"" class=""btn btn-sm btn-outline-primary""");
            BeginWriteAttribute("onclick", " onclick=\"", 1495, "\"", 1561, 3);
            WriteAttributeValue("", 1505, "location.href=\'", 1505, 15, true);
#nullable restore
#line 29 "E:\Wilson Sanchez Personal\Universidad Fidelitas\Proyecto graduacion\GestorProyecto\GestorProyecto\GestorProyecto\Views\Hospital\all-doctors.cshtml"
WriteAttributeValue("", 1520, Url.Action("doctor-profile","hospital"), 1520, 40, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1560, "\'", 1560, 1, true);
            EndWriteAttribute();
            WriteLiteral(@">View Profile</button>
            </div><!--end card-body-->
        </div><!--end card-->
    </div><!--end col-->
    <div class=""col-lg-3"">
        <div class=""card"">
            <div class=""card-body text-center"">
                <div class=""text-right"">
                    <a href=""#"" class=""mr-2"" data-toggle=""modal"" data-animation=""bounce"" data-target="".bs-example-modal-lg""><i class=""fas fa-edit text-info font-16""></i></a>
                    <a href=""#""><i class=""fas fa-trash-alt text-danger font-16""></i></a>
                </div>
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "1c500bdee8b37fc4d0dff9e843fc5dfe1bc7b29b9744", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                <h5 class=""mb-1 client-name"">Dr.Helen White</h5>
                <p class=""text-muted"">MS Cardiology</p>
                <p class=""text-center font-14"">3 years experience in Apollo Hospital</p>
                <button type=""button"" class=""btn btn-sm btn-outline-primary""");
            BeginWriteAttribute("onclick", " onclick=\"", 2501, "\"", 2567, 3);
            WriteAttributeValue("", 2511, "location.href=\'", 2511, 15, true);
#nullable restore
#line 44 "E:\Wilson Sanchez Personal\Universidad Fidelitas\Proyecto graduacion\GestorProyecto\GestorProyecto\GestorProyecto\Views\Hospital\all-doctors.cshtml"
WriteAttributeValue("", 2526, Url.Action("doctor-profile","hospital"), 2526, 40, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 2566, "\'", 2566, 1, true);
            EndWriteAttribute();
            WriteLiteral(@">View Profile</button>
            </div><!--end card-body-->
        </div><!--end card-->
    </div><!--end col-->
    <div class=""col-lg-3"">
        <div class=""card"">
            <div class=""card-body text-center"">
                <div class=""text-right"">
                    <a href=""#"" class=""mr-2"" data-toggle=""modal"" data-animation=""bounce"" data-target="".bs-example-modal-lg""><i class=""fas fa-edit text-info font-16""></i></a>
                    <a href=""#""><i class=""fas fa-trash-alt text-danger font-16""></i></a>
                </div>
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "1c500bdee8b37fc4d0dff9e843fc5dfe1bc7b29b12447", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                <h5 class=""mb-1 client-name"">Dr.Thomas Fant</h5>
                <p class=""text-muted"">MD Neurology</p>
                <p class=""text-center font-14"">10 years experience in Apollo Hospital</p>
                <button type=""button"" class=""btn btn-sm btn-outline-primary""");
            BeginWriteAttribute("onclick", " onclick=\"", 3507, "\"", 3573, 3);
            WriteAttributeValue("", 3517, "location.href=\'", 3517, 15, true);
#nullable restore
#line 59 "E:\Wilson Sanchez Personal\Universidad Fidelitas\Proyecto graduacion\GestorProyecto\GestorProyecto\GestorProyecto\Views\Hospital\all-doctors.cshtml"
WriteAttributeValue("", 3532, Url.Action("doctor-profile","hospital"), 3532, 40, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3572, "\'", 3572, 1, true);
            EndWriteAttribute();
            WriteLiteral(@">View Profile</button>
            </div><!--end card-body-->
        </div><!--end card-->
    </div><!--end col-->
    <div class=""col-lg-3"">
        <div class=""card"">
            <div class=""card-body text-center"">
                <div class=""text-right"">
                    <a href=""#"" class=""mr-2"" data-toggle=""modal"" data-animation=""bounce"" data-target="".bs-example-modal-lg""><i class=""fas fa-edit text-info font-16""></i></a>
                    <a href=""#""><i class=""fas fa-trash-alt text-danger font-16""></i></a>
                </div>
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "1c500bdee8b37fc4d0dff9e843fc5dfe1bc7b29b15151", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                <h5 class=""mb-1 client-name"">Dr.Justin Williams</h5>
                <p class=""text-muted"">MS Psychology</p>
                <p class=""text-center font-14"">4 years experience in Apollo Hospital</p>
                <button type=""button"" class=""btn btn-sm btn-outline-primary""");
            BeginWriteAttribute("onclick", " onclick=\"", 4517, "\"", 4583, 3);
            WriteAttributeValue("", 4527, "location.href=\'", 4527, 15, true);
#nullable restore
#line 74 "E:\Wilson Sanchez Personal\Universidad Fidelitas\Proyecto graduacion\GestorProyecto\GestorProyecto\GestorProyecto\Views\Hospital\all-doctors.cshtml"
WriteAttributeValue("", 4542, Url.Action("doctor-profile","hospital"), 4542, 40, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 4582, "\'", 4582, 1, true);
            EndWriteAttribute();
            WriteLiteral(@">View Profile</button>
            </div><!--end card-body-->
        </div><!--end card-->
    </div><!--end col-->
</div><!--end row-->

<div class=""row"">
    <div class=""col-lg-3"">
        <div class=""card"">
            <div class=""card-body text-center"">
                <div class=""text-right"">
                    <a href=""#"" class=""mr-2"" data-toggle=""modal"" data-animation=""bounce"" data-target="".bs-example-modal-lg""><i class=""fas fa-edit text-info font-16""></i></a>
                    <a href=""#""><i class=""fas fa-trash-alt text-danger font-16""></i></a>
                </div>
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "1c500bdee8b37fc4d0dff9e843fc5dfe1bc7b29b17904", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                <h5 class=""mb-1 client-name"">Dr.Wendy Keen</h5>
                <p class=""text-muted"">MD Orthopedic</p>
                <p class=""text-center font-14"">4 years experience in Apollo Hospital</p>
                <button type=""button"" class=""btn btn-sm btn-outline-primary""");
            BeginWriteAttribute("onclick", " onclick=\"", 5565, "\"", 5631, 3);
            WriteAttributeValue("", 5575, "location.href=\'", 5575, 15, true);
#nullable restore
#line 92 "E:\Wilson Sanchez Personal\Universidad Fidelitas\Proyecto graduacion\GestorProyecto\GestorProyecto\GestorProyecto\Views\Hospital\all-doctors.cshtml"
WriteAttributeValue("", 5590, Url.Action("doctor-profile","hospital"), 5590, 40, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 5630, "\'", 5630, 1, true);
            EndWriteAttribute();
            WriteLiteral(@">View Profile</button>
            </div><!--end card-body-->
        </div><!--end card-->
    </div><!--end col-->
    <div class=""col-lg-3"">
        <div class=""card"">
            <div class=""card-body text-center"">
                <div class=""text-right"">
                    <a href=""#"" class=""mr-2"" data-toggle=""modal"" data-animation=""bounce"" data-target="".bs-example-modal-lg""><i class=""fas fa-edit text-info font-16""></i></a>
                    <a href=""#""><i class=""fas fa-trash-alt text-danger font-16""></i></a>
                </div>
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "1c500bdee8b37fc4d0dff9e843fc5dfe1bc7b29b20607", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                <h5 class=""mb-1 client-name"">Dr.Helen White</h5>
                <p class=""text-muted"">MS Cardiology</p>
                <p class=""text-center font-14"">3 years experience in Apollo Hospital</p>
                <button type=""button"" class=""btn btn-sm btn-outline-primary""");
            BeginWriteAttribute("onclick", " onclick=\"", 6571, "\"", 6637, 3);
            WriteAttributeValue("", 6581, "location.href=\'", 6581, 15, true);
#nullable restore
#line 107 "E:\Wilson Sanchez Personal\Universidad Fidelitas\Proyecto graduacion\GestorProyecto\GestorProyecto\GestorProyecto\Views\Hospital\all-doctors.cshtml"
WriteAttributeValue("", 6596, Url.Action("doctor-profile","hospital"), 6596, 40, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 6636, "\'", 6636, 1, true);
            EndWriteAttribute();
            WriteLiteral(@">View Profile</button>
            </div><!--end card-body-->
        </div><!--end card-->
    </div><!--end col-->
    <div class=""col-lg-3"">
        <div class=""card"">
            <div class=""card-body text-center"">
                <div class=""text-right"">
                    <a href=""#"" class=""mr-2"" data-toggle=""modal"" data-animation=""bounce"" data-target="".bs-example-modal-lg""><i class=""fas fa-edit text-info font-16""></i></a>
                    <a href=""#""><i class=""fas fa-trash-alt text-danger font-16""></i></a>
                </div>
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "1c500bdee8b37fc4d0dff9e843fc5dfe1bc7b29b23312", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                <h5 class=""mb-1 client-name"">Dr.Thomas Fant</h5>
                <p class=""text-muted"">MD Neurology</p>
                <p class=""text-center font-14"">10 years experience in Apollo Hospital</p>
                <button type=""button"" class=""btn btn-sm btn-outline-primary""");
            BeginWriteAttribute("onclick", " onclick=\"", 7577, "\"", 7643, 3);
            WriteAttributeValue("", 7587, "location.href=\'", 7587, 15, true);
#nullable restore
#line 122 "E:\Wilson Sanchez Personal\Universidad Fidelitas\Proyecto graduacion\GestorProyecto\GestorProyecto\GestorProyecto\Views\Hospital\all-doctors.cshtml"
WriteAttributeValue("", 7602, Url.Action("doctor-profile","hospital"), 7602, 40, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 7642, "\'", 7642, 1, true);
            EndWriteAttribute();
            WriteLiteral(@">View Profile</button>
            </div><!--end card-body-->
        </div><!--end card-->
    </div><!--end col-->
    <div class=""col-lg-3"">
        <div class=""card"">
            <div class=""card-body text-center"">
                <div class=""text-right"">
                    <a href=""#"" class=""mr-2"" data-toggle=""modal"" data-animation=""bounce"" data-target="".bs-example-modal-lg""><i class=""fas fa-edit text-info font-16""></i></a>
                    <a href=""#""><i class=""fas fa-trash-alt text-danger font-16""></i></a>
                </div>
                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "1c500bdee8b37fc4d0dff9e843fc5dfe1bc7b29b26017", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                <h5 class=""mb-1 client-name"">Dr.Justin Williams</h5>
                <p class=""text-muted"">MS Psychology</p>
                <p class=""text-center font-14"">4 years experience in Apollo Hospital</p>
                <button type=""button"" class=""btn btn-sm btn-outline-primary""");
            BeginWriteAttribute("onclick", " onclick=\"", 8587, "\"", 8653, 3);
            WriteAttributeValue("", 8597, "location.href=\'", 8597, 15, true);
#nullable restore
#line 137 "E:\Wilson Sanchez Personal\Universidad Fidelitas\Proyecto graduacion\GestorProyecto\GestorProyecto\GestorProyecto\Views\Hospital\all-doctors.cshtml"
WriteAttributeValue("", 8612, Url.Action("doctor-profile","hospital"), 8612, 40, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 8652, "\'", 8652, 1, true);
            EndWriteAttribute();
            WriteLiteral(">View Profile</button>\r\n            </div><!--end card-body-->\r\n        </div><!--end card-->\r\n    </div><!--end col-->\r\n</div><!--end row-->\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
