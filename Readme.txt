Nombre del proyecto: Sitio Web Transaccional

Integrantes:
Lucianny Castro Monge
Sofia Cascante Sánchez
Ingrid Herra Arroyo
Wilson Sánchez Guzmán

Descripción del proyecto
Este proyecto se quiere diseñar un sistema que le permita a la empresa tener un control más detallado y simple de la gestión de proyectos de la empresa, 
que le permitirá administrar de forma sencilla los tiempos y disponibilidad del personal de empresa, además mejor los tiempos de desarrollo o implementación
de los proyectos de la empresa.

Módulo de Revisión de Sugerencias: En este módulo se desea cargar las sugerencias que se registran en los diferentes sistemas de la empresa por parte de los clientes,
y poder darles un seguimiento, respuesta al cliente, y definición de nuevos desarrollos. 
                                                                                                                                                                                                                                                                                               
Módulo de Proyectos y Seguimiento: Se van a administrar los diferentes proyectos de la empresa, poder visualizar avances, tiempos invertidos, inicio 
y finalización de los proyectos y demás información relacionada con los proyectos.

Módulo de Registro y Gestión Tiquetes: Brindar una mesa de ayuda para el cliente, para que pueda registrar los incidentes de los diferentes sistemas de la empresa 
y poder darle un seguimiento a los mismos.

Módulo de Tareas y Seguimiento: Realizar el registro de tareas, tiempos de duración en las diferentes tareas y administración de tareas del empleado de día a día.  



Cómo instalar el repositorio en el equipo para desarrollo

1-	Para crear un repositorio en Bitbucket debemos tener instalado GIT o bien el cliente de Bitbucket Atlassian Sourcetree 
(siendo esta ultima la que se utilizara ya que los repositorios solo los manejaremos mediante Bitbucket.) y además debemos tener 
una cuenta en Bitbucket.

2-	Para instalar el cliente Sourcetree primero debemos descargarlo en el sigunete link https://www.sourcetreeapp.com/ 
después de descargar el aplicativo, debemos crear nuestro usuario en el siguiente link: https://id.atlassian.com/ aquí nos podemos registrar y 
con esto ya podemos proceder a instalar Sourcetree.

3-	Lo primero será ejecutar el .exe SourceTreeSetup-3.4.5.exe como administrador.

4-	Luego de ejecutarlo nos mostrara una pantalla en la que seleccionamos la opción bitbucket.

5-	Si en nuestro navegador principal ya tenemos inicia nuestra sesión de bitbucket iniciada, el programa abrirá nuestro navegador y 
nos solicitará acceso a nuestra cuenta en este caso le damos acceso.

6-	Ya con esto nuestro registro se completará.

7-	 Al dar Next este nos solicitara instalar varias herramientas por lo cual procedemos a dar next.

8-	Este descargara las aplicaciones requeridas y las instalara, cuando finaliza le damos en next.

9-	Lo siguiente nos solicitara un correo y nuestro nombre de usuario, si el registro se realizo correcta mente esta parte lo agregara 
automáticamente y le damos next.

10-	Luego nos solicitara que carguemos una key ssh,  le damos en no y el proceso de instalación se realizara.

11-	Ya con esto tenemos todo listo para crear nuestro repositorio.

12-	Ya con esto tenemos todo listo para crear nuestro repositorio.

13-	Asegurarse que la carpeta donde se alojara nuestro repositorio su nombre este en minúsculas y sin espacios, si no mostrara un error, 
con esto presente seleccionamos la carpeta, ingresamos el nombre, seleccionamos git, seguido seleccionamos el check créate  repository on account. 
Al crear el repositorio ya podremos subir nuestros proyectos en nuestro repositorio local y actualizarlo en nuestra cuenta.

14-	Seleccionamos nuestro repositorio, para poder subir nuestros archivos a nuestro repositorio debemos almacenar nuestro prototipo en nuestra 
capeta local ya seleccionada.

15-	Con esto vamos a poder ver nuestros archivos por consíguete podremos subirlo para eso se selecciona satage all, y 
luego se le da una descripción en el cudro de abajo para dar comit y crear el master.

16-	Ya para finalizar seleccionamos la opción push, la cual nos abrirá una ventana, seleccionamos el master y seleccionamos nuevamente en push, 
y se espera hasta que el prototipo este subido.

17-	También nos pedirá las credenciales, por lo cual se ponen y se da clic en login.


18-	Ya con esto el repositorio se subió y podemos visualizarlo tanto en local como en la cuenta.

Para poder acceder a este repositorio se puede usar el siguiente link: https://bitbucket.org/Wsanchez00244/gestorproyecto/src/master/


